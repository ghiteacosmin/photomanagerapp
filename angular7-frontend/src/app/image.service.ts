import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  constructor(private http: HttpClient) { }

  uploadImage(file: File, tags: Array<string>, description: string, location: string, date) {
    const fd = new FormData();
    fd.append('file', file, file.name);
    tags.forEach((elem) => {
      fd.append('tags', elem);
    });
    fd.append('description', description);
    fd.append('location', location);
    fd.append('date', date);
    return this.http.post('/api/uploadImage', fd);
  }

  getImageData(
    page?: Number | undefined,
    tags?: string[] | undefined,
    location?: string | undefined,
    fromData?: Date | undefined,
    toData?: Date | undefined,
    description?: string | undefined,
    sortDate?: boolean | undefined,
    sortLocation?: boolean | undefined
  ) {
    const defaultPage = page ? page : 1;
    let defaultToDate: Date | undefined = fromData ? new Date() : undefined;
    let defaultFromDate: Date | undefined = toData ? new Date(0) : undefined;

    return this.http.post('/api/getImages', {
      page: defaultPage,
      tags,
      location,
      fromData: defaultFromDate,
      toData: defaultToDate,
      description,
      sortDate,
      sortLocation
    });
  }

  updateRating(stars, imageID) {
    return this.http.post('/api/addRatting', {
      stars,
      imageID
    });
  }

  addComment(comment, imageID) {
    return this.http.post('/api/addComment', {
      comment,
      imageID
    });
  }
}
