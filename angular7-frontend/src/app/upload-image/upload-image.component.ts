import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';
import { ImageService } from '../image.service';

@Component({
  selector: 'app-upload-image',
  templateUrl: './upload-image.component.html',
  styleUrls: ['./upload-image.component.scss']
})
export class UploadImageComponent implements OnInit {

  selectedFile = null;
  imageSrc;
  success = false;
  error: string;
  
  constructor(private auth: AuthenticationService, private imageService: ImageService, private router: Router) { }

  ngOnInit() {
    if (!this.auth.isLoggedIn()) {
      this.router.navigate(['login']);
    }
  }

  onFileSelected(event) {
    if (event.target.files && event.target.files[0]) {
      const file = event.target.files[0];

      const reader = new FileReader();
      reader.onload = e => this.imageSrc = reader.result;

      reader.readAsDataURL(file);
    }
    this.selectedFile = event.target.files[0];
  }

  uploadFile(description, location, tags1:string, tags2:string, date, formData) {
    const constructTagArray: Array<string> = tags2 ? <Array<string>>[tags1, tags2] : <Array<string>>[tags1];
    this.imageService.uploadImage(this.selectedFile, constructTagArray, description, location, date)
      .subscribe(() => {
        this.success = true;
        formData.resetForm();
      }, (error) => {
        if (error.status >= 500) {
          return this.error = 'There is a server problem';
        }
        return this.error = error.error.message;
      })
  }

}
