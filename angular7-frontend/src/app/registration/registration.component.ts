import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.scss']
})
export class RegistrationComponent implements OnInit {

  error: string;
  constructor(private auth: AuthenticationService, private router: Router) { }

  ngOnInit() {
    if (this.auth.isLoggedIn()) {
      this.router.navigate(['dashboard']);
    }
  }

  onSubmit(name: string, email: string, password: string) {
    this.auth.registration(email, password, name)
    .subscribe(() => {
      return this.router.navigate(['login']);
    }, (error) => {
      if (error.status >= 500) {
        return this.error = 'There is a server problem';
      }
      return this.error = error.error.message;
    })
  }

}
