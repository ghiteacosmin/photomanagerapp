export interface IComments {
  description: string,
  name: string,
  email: string,
}