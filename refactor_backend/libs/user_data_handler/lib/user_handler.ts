import {MongoService} from '../../mongo_service';


export class UserDataHandler {
  mongoService: MongoService;

  constructor() {
    this.mongoService = new MongoService();
  }

  async registration(email: string, name: string, password: string) {
    try {
      await this.mongoService.addNewUser(email, name, password);
      return {
        statusCode: 200,
        message: 'Authentication successful!'
      }
    } catch (err) {
      return {
        statusCode: 400,
        message: err.message,
      }
    }
  }

  async login(email: string, password: string) {
    try {
      const userData = await this.mongoService.checkUserExist(email, password);
      return {
        statusCode: 200,
        message: 'Authentication successful!',
        userData
      }
    } catch (err) {
      return {
        statusCode: 404,
        message: err.message,
      }
    }
  }

}
