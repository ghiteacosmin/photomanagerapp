import {run} from 'newman'
import Bluebird from 'bluebird';

// const postmanJson = require('./Postman_collection_test.json');

const newmanRun = Bluebird.promisify(run);

describe.skip('Integration testing', () => {

  it('should not fail any of the integration test', function createMock() {
    this.timeout(0);
    return newmanRun({
      collection: 'postmanJson',
      reporters: 'cli',
      insecure: true,
    });
  });
});