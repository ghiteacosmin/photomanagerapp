import { expect } from 'chai';
import 'mocha';
import proxyquire from 'proxyquire';
import { UserModel } from '../../mocks/userModel.mock';
import { bcryptCompare } from '../../mocks/bcryptCompare.mock';

describe('MongoService class', () => {
  const MongoService = proxyquire('../../../libs/mongo_service/lib/mongo_service.ts', {
    '../../mongo_repository': { UserModel },
    bcryptjs: bcryptCompare
  });

  const mongoService = new MongoService.MongoService();

  beforeEach((done) => {
    // TODO something before testing
    done();
  })

  afterEach((done) => {
    // TODO something after testing
    done();
  })

  describe('checkUserExist method', () => {

    it('should throw error if the user was not found', () => {
      return mongoService.checkUserExist('error@yahoo.com', 'secret')
        .then(() => {
          expect.fail('Unexpected resolve');
        })
        .catch((err: any) => {
          expect(err.message).to.equal('Incorrect username or password');
        });
    });

    it('should throw error if password is not correct', () => {
      return mongoService.checkUserExist('test@yahoo.com', 'error')
        .then(() => {
          expect.fail('Unexpected resolve');
        })
        .catch((err: any) => {
          expect(err.message).to.equal('Incorrect username or password');
        });
    });

    it('should resolve with an object of email and name', () => {
      return mongoService.checkUserExist('test@yahoo.com', 'secret')
        .then((result: any) => {
          expect(result.email).to.equal('test@yahoo.com');
          expect(result.name).to.equal('test')
        });
    });
  });
});