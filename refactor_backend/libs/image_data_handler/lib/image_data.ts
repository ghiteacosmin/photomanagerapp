import { MongoService } from "../../mongo_service";

export class ImageDataHandler {
  mongoService: MongoService;

  constructor() {
    this.mongoService = new MongoService();
  }

  async uploadImage(
    tags: Array<string>,
    description: string,
    location: string,
    date: Date,
    name: string,
    email: string,
    imageName: string) {
    try {
      await this.mongoService.addNewImage(imageName, name, email, tags, description, location, date);
      return {
        statusCode: 200,
        message: 'Image Uploaded'
      }
    } catch (err) {
      return {
        statusCode: 400,
        message: err.message,
      }
    }
  }

  async addUpdateRating(imageName: string, stars: number, name: string, email: string) {
    try {
      const starAverage = await this.mongoService.addUpdateImageStarRating(imageName, stars, name, email);
      return {
        statusCode: 200,
        message: 'Image Updated',
        averageStar: starAverage
      }
    } catch (err) {
      return {
        statusCode: 404,
        message: err.message,
      }
    }
  }

  async addComments(imageName: string, comment: string, name: string, email: string) {
    try {
      await this.mongoService.addNewComment(imageName, comment, name, email);
      return {
        statusCode: 200,
        message: 'Image Updated',
      }
    } catch (err) {
      return {
        statusCode: 404,
        message: err.message,
      }
    }
  }

  async getImages(
    page?: number,
    tags?: string[],
    location?: string,
    fromData?: Date,
    toData?: Date,
    description?: string,
    sortDate?: boolean,
    sortLocation?: boolean) {
    try {
      const imageData = await this.mongoService.returnImages(
        page, 
        tags, 
        location,
        fromData,
        toData,
        description,
        sortDate,
        sortLocation);
      return {
        statusCode: 200,
        message: 'Image Updated',
        imageData
      }
    } catch (err) {
      return {
        statusCode: 404,
        message: err.message,
      }
    }
  }

}
