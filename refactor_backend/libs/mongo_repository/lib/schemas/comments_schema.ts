import { Schema } from 'mongoose';

export const CommentsSchema = new Schema({
  description: {
    type: String,
    minlength: 0,
    maxlength: 255,
  },
  name: {
    type: String,
    minlength: 5,
    maxlength: 50
  },
  email: {
    type: String,
    minlength: 5,
    maxlength: 255,
  },
});
