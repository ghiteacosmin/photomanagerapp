import { Schema } from 'mongoose';

export const StarRatingsSchema = new Schema({
  stars: {
    type: Number,
    min: 0,
    max: 5,
  },
  name: {
    type: String,
    minlength: 5,
    maxlength: 50
  },
  email: {
    type: String,
    minlength: 5,
    maxlength: 255,
   },
});
