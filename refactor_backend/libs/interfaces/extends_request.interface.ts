import { Request } from 'express';

export interface IExtendedRequest extends Request {
  decode: string | object,
  headers: {
		authorization: string
	},
	body: {
		tags: Array<string>,
		description: string,
		location: string
		imageID: string,
		stars: number,
		date: Date
	}
}
