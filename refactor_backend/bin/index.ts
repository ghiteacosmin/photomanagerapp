import { ExpressServer } from "../libs";

const debug = require('debug')('photo_manager_rest_api:http-server');

const app = new ExpressServer();
app.startServer()
.then((response) => {
  debug('--------------------------------');
  debug('The server started successfully');
  debug(`The response is: ${response}`)
  debug('--------------------------------');
})
.catch((err) => {
  debug('--------------------------------');
  debug('Something went wrong');
  debug(`The err is: ${err}`)
  debug('--------------------------------');
});
