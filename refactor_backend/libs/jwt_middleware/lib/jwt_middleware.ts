import { Response, NextFunction } from 'express';
import {verify, sign} from 'jsonwebtoken';
import { IExtendedRequest } from '../../interfaces';
import Bluebird from 'bluebird';
import { IApiConfig } from '../../interfaces';

const apiConfig: IApiConfig = require('../../api_config/api_config.json');

export class JWTMiddleware {
  checkToken(req: IExtendedRequest, res: Response, next: NextFunction) {
    return Bluebird.try(() => {
      let token: string = <string>(req.headers['authorization'] || '');

      if (token && token.startsWith('Bearer ')) {
        token = token.slice(7, token.length);
      } else {
        return res.status(404).json({
          message: 'The authorization header was not provided'
        })
      }

      return verify(token, apiConfig.jwtSecretKey, (err, decoded) => {
        if (err) {
          return res.status(404).json({
						message: 'The authorization header was not provided'
					})
        } else {
          req.decode = decoded;
          return next();
        }
      });
    });
  }

  decodeToken(jwtToken: string) {
    return verify(jwtToken, apiConfig.jwtSecretKey);
  }

  generateToken(email: string, name: string) {
    return sign({email, name}, apiConfig.jwtSecretKey);
  }
}
