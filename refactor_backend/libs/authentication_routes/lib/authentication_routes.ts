import { Router, Request, Response } from 'express';
import { UserDataHandler } from '../../user_data_handler';
import { JWTMiddleware } from '../../jwt_middleware';

export class AuthenticationRoutes {

  router: Router;
  userDataHandler: UserDataHandler;
  jwtMiddleware: JWTMiddleware;

  constructor() {
    this.router = Router();
    this.userDataHandler = new UserDataHandler();
    this.jwtMiddleware = new JWTMiddleware();
    this.initialize();
  }

  initialize() {
    this.initializeLogin();
    this.initializeRegistration();
  }

  initializeRegistration() {
    return this.router.post('/registration', async (req: Request, res: Response) => {
      try {
        const { email, name, password } = req.body;
        const result = await this.userDataHandler.registration(email, name, password);
        return res.status(result.statusCode).json({
          message: result.message,
        });
      } catch (err) {
        return res.status(500).json({
          message: err.message,
        });
      }
    })
  }

  initializeLogin() {
    return this.router.post('/login', async (req: Request, res: Response) => {
      try {
        const { email, password } = req.body;
        const result = await this.userDataHandler.login(email, password);
        let jwtToken = undefined
        if (result.userData) {
          jwtToken = await this.jwtMiddleware.generateToken(result!.userData!.email, result!.userData!.name);
        }
        return res.status(result.statusCode).json({
          message: result.message,
          token: jwtToken
        });
      } catch (err) {
        return res.status(500).json({
          message: err.message,
        });
      }
    })
  }

}