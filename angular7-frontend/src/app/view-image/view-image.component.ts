import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';
import { ImageService } from '../image.service';

@Component({
  selector: 'app-view-image',
  templateUrl: './view-image.component.html',
  styleUrls: ['./view-image.component.scss']
})
export class ViewImageComponent implements OnInit {

  imagesData;
  initialPage = 1;

  locationSearch = undefined;
  descriptionSearch = undefined;
  tagSearch = undefined;

  dateSort = false;
  locationSort = false;

  constructor(private auth: AuthenticationService, private imageService: ImageService, private router: Router) { }

  ngOnInit() {
    if (!this.auth.isLoggedIn()) {
      this.router.navigate(['login']);
    }
    this.imageService.getImageData()
      .subscribe((result) => {
        this.imagesData = result;
      });
  }

  reloadPage() {
    location.reload();
  }

  getStars(event, imageName) {
    const value = event.rating;
    return this.imageService.updateRating(value, imageName)
      .subscribe((data) => {
        this.imageService.getImageData(this.initialPage, this.tagSearch, this.locationSearch, undefined, undefined, this.descriptionSearch, this.dateSort, this.locationSort)
          .subscribe((result) => {
            this.imagesData = result;
          });
      })
  }

  descriptionOnChange(value) {
    this.descriptionSearch = value;
  }

  tagsOnChange(value) {
    if (value) {
      this.tagSearch = [value];
    } else {
      this.tagSearch = undefined;
    }
  }

  locationOnChange(value) {
    this.locationSearch = value;
  }

  searchButton() {
    this.imageService.getImageData(this.initialPage, this.tagSearch, this.locationSearch, undefined, undefined, this.descriptionSearch, this.dateSort, this.locationSort)
      .subscribe((result) => {
        this.imagesData = result;
      });
  }

  sortByDate(value) {
    if (value !== '') {
      this.dateSort = !value;
    } else {
      this.dateSort = true;
    }
    return this.imageService.getImageData(this.initialPage, this.tagSearch, this.locationSearch, undefined, undefined, this.descriptionSearch, this.dateSort, this.locationSort)
      .subscribe((result) => {
        this.imagesData = result;
      });
  }

  sortByLocation(value) {
    if (value !== '') {
      this.locationSort = !value;
    } else {
      this.locationSort = true;
    }
    return this.imageService.getImageData(this.initialPage, this.tagSearch, this.locationSearch, undefined, undefined, this.descriptionSearch, this.dateSort, this.locationSort)
      .subscribe((result) => {
        this.imagesData = result;
      });
  }

  addCommnet(value, imageName) {
    return this.imageService.addComment(value, imageName).subscribe(() => {
      this.imageService.getImageData(this.initialPage, this.tagSearch, this.locationSearch, undefined, undefined, this.descriptionSearch, this.dateSort, this.locationSort)
        .subscribe((result) => {
          this.imagesData = result;
        });
    })
  }

  goToNewPage() {
    this.imageService.getImageData(this.initialPage + 1, this.tagSearch, this.locationSearch, undefined, undefined, this.descriptionSearch, this.dateSort, this.locationSort)
      .subscribe((result: any) => {
        if (result.length > 0) {
          this.initialPage++;
          return this.imagesData = result;
        }
      });
  }

  goBackPage() {
    this.initialPage--;
    this.imageService.getImageData(this.initialPage, this.tagSearch, this.locationSearch, undefined, undefined, this.descriptionSearch, this.dateSort, this.locationSort)
      .subscribe((result) => {
        this.imagesData = result;
      });
  }

}
