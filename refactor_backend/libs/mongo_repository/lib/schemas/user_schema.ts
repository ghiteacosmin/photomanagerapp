import { Schema } from 'mongoose';
import { hash, genSalt } from 'bcryptjs';

import { IUserModel } from '../../../interfaces';

async function hashPassword(password: string) {
  try {
    const salt = await genSalt(10);
    return hash(password, salt)
  } catch (err) {
    throw new Error(err);
  }
}

export const UserSchema = new Schema({
  name: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 50
  },
  email: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 255,
    unique: true
  },
  password: {
    type: String,
    required: true,
    minlength: 5,
    maxlength: 1024
  },
});


UserSchema.pre('save', async function (next) {
  const user = <IUserModel>this;

  if (!this.isModified('password')) {
    return next();
  }

  const passwordHash = await hashPassword(user.password);
  user.password = passwordHash;
  return next();
});
