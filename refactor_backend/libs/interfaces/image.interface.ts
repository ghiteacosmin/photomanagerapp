import { Document } from 'mongoose';
import { IComments } from './comment.interface';
import { IStarRating } from './star_rating.interface';

export interface IImage {
  name: string,
  email: string;
  img: string;
  tags: string[];
  description: string;
  location: string;
  date: Date;
  comments: Array<IComments>
  starRating: Array<IStarRating>;
  averageStar: number;
}

export interface IImageModel extends IImage, Document {}
