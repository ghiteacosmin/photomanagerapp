import { IApiConfig } from '../../interfaces/api_config.interface';
import Bluebird from 'bluebird';
import mongoose, { Mongoose } from 'mongoose';
import express from 'express';
import { AuthenticationRoutes } from '../../authentication_routes';
import { urlencoded, json } from 'body-parser';
import { DashboardRoutes } from '../../dashboard_routes';

const apiConfig: IApiConfig = require('../../api_config/api_config.json');

(<any>mongoose).Promise = Bluebird;

export class ExpressServer {
  
  mongoConnection: Mongoose | null = null;
  authenticationRoutes: AuthenticationRoutes | any;
  dashboardRoutes: DashboardRoutes | any;

  constructor() {
    this.authenticationRoutes = new AuthenticationRoutes();
    this.dashboardRoutes = new DashboardRoutes();
  }

  async startServer() {
    try {
      this.mongoConnection = await mongoose.connect(apiConfig.mongoUrl, {
        useNewUrlParser: true,
        useCreateIndex: true,
      })
      const app = express();
      app.use(urlencoded({extended: true}));
      app.use(json());
      app.use('/api/photos', 
        this.authenticationRoutes.jwtMiddleware.checkToken,
        express.static(apiConfig.photosPath));
      app.use('/api', this.authenticationRoutes.router);
      app.use('/api', this.dashboardRoutes.jwtMiddleware.checkToken, this.dashboardRoutes.router);
      app.listen(apiConfig.serverPort);
    } catch(err) {
      this.mongoConnection!.disconnect();
      throw err;
    }
  }
}