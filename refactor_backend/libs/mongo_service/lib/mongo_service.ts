import { UserModel, ImageModel } from '../../mongo_repository';
import { isNil, isEmpty } from 'ramda';
import { compare } from 'bcryptjs';
import { IStarRating, IImageModel } from '../../interfaces';

export class MongoService {

  async addNewUser(email: string, name: string, password: string) {
    const userData = await UserModel.findOne({ email });
    if (!isNil(userData)) {
      throw new Error('User already exist');
    }
    const newUserData = new UserModel({
      name,
      email,
      password
    });

    return newUserData.save();
  }

  async checkUserExist(email: string, password: string) {

    const userDate = await UserModel.findOne({ email });

    if (isNil(userDate)) {
      throw new Error('Incorrect username or password');
    }

    const passwordVerify = await compare(password, userDate.password);

    if (!passwordVerify) {
      throw new Error('Incorrect username or password');
    }

    return {
      email: userDate.email,
      name: userDate.name,
    };
  }

  addNewImage(
    img: string,
    name: string,
    email: string,
    tags: Array<string>,
    description: string,
    location: string,
    date: Date) {
    const newImage = new ImageModel({
      img, name, email, tags, description, location, date
    });
    return newImage.save();
  }


  private calculateStarAverage(starRating: Array<IStarRating>) {
    const constructArray = starRating.map((element) => {
      return element.stars;
    });

    const sumStars = constructArray.reduce((acc, value) => {
      return acc + value;
    });

    return sumStars / constructArray.length;
  }

  async addUpdateImageStarRating(img: string, stars: number, name: string, email: string) {
    const imageData = await ImageModel.findOne({ img }).exec();
    const updateData = {
      stars,
      name,
      email,
    }

    if (isNil(imageData)) {
      throw new Error('Image Not Found');
    }

    const existRating = imageData.starRating.some((el) => {
      if (el.email === email) {
        el.stars = stars;
        return true;
      }
      return false;
    });

    if (!existRating) {
      imageData.starRating.push(updateData);
    }

    imageData.averageStar = this.calculateStarAverage(imageData.starRating);
    await imageData.save();
    return imageData.averageStar;
  }

  async addNewComment(img: string, description: string, name: string, email: string) {
    const imageData = await ImageModel.findOne({ img }).exec();
    if (isNil(imageData)) {
      throw new Error('Image Not Found');
    }

    imageData.comments.push({ description, name, email });
    return imageData.save();
  }

  async returnImages(
    page?: number,
    tags?: string[],
    location?: string,
    fromData?: Date,
    toData?: Date,
    description?: string,
    sortDate?: boolean,
    sortLocation?: boolean,
  ) {
    const defaultPage = isNil(page) ? 1 : page;
    const searchObject: IImageModel | any = {};
    const filterObject: any = {
      skip: 5 * (defaultPage - 1),
    }

    if (sortDate) {
      filterObject.sort = {
        date: 'desc'
      }
    }

    if (sortLocation) {
      if (sortDate) {
        filterObject.sort.location = 'asc';
      } else {
        filterObject.sort = {
          location: 'asc'
        }
      }
    }

    if (!isNil(tags) && !isEmpty(tags)) {
      searchObject.tags = {
        $in: tags
      }
    }

    if (!isNil(location) && !isEmpty(location)) {
      searchObject.location = location;
    }

    if (!isNil(description)) {
      const regex = new RegExp(description!)
      searchObject.description = regex
    }

    if (fromData && toData) {
      searchObject.date = {
        $gte: new Date(fromData),
        $lt: new Date(toData)
      }
    }

    return ImageModel.find(searchObject, null, filterObject)
      .limit(5 * defaultPage)
      .exec();
  }

}
