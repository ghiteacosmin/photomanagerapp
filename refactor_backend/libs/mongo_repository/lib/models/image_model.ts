import { model, Model } from 'mongoose';
import { IImageModel } from '../../../interfaces';
import { ImageSchema } from '../schemas/image_schema';

export const ImageModel: Model<IImageModel> = model<IImageModel>('ImageModel', ImageSchema);
