import { Router, Response, NextFunction } from 'express';
import { JWTMiddleware } from '../../jwt_middleware';
import { ImageDataHandler } from '../../image_data_handler';
import multer from 'multer';
import { v4 } from 'uuid';
import { exists, mkdir } from 'fs';
import { IApiConfig } from '../../interfaces';

const apiConfig: IApiConfig = require('../../api_config/api_config.json');

export class DashboardRoutes {

	router: Router;
	imageDataHandler: ImageDataHandler;
	jwtMiddleware: JWTMiddleware;

	constructor() {
		this.router = Router();
		this.imageDataHandler = new ImageDataHandler();
		this.jwtMiddleware = new JWTMiddleware();
		this.initialize();
	}

	initialize() {
		this.initializeAddUpdateRating();
		this.initializeUpdateImage();
		this.initializeAddComment();
		this.initializeGetImageData();
	}

	private checkIfFolderExists(req: Request, res: Response, next: NextFunction) {
		exists(apiConfig.photosPath, function (exists) {
			if (exists) {
				next();
			}
			else {
				mkdir(apiConfig.photosPath, function (err) {
					if (err) {
						next();
					}
					next();
				})
			}
		})
	}

	private multerMiddlewareUploadImage() {
		const storage = multer.diskStorage({
			destination: function (req, res, cb) {
				cb(null, `${apiConfig.photosPath}/`)
			},
			filename: function (req, file, cb) {
				if (!file.originalname.match(/\.(png|jpeg|jpg|wav|tif|gif)$/)) {
					const err = new Error('Please add a specific error');
					return cb(err, '');
				} else {
					const photoUUID = v4()
					const extension = file.originalname.split('.');
					cb(null, Date.now() + "_" + `${photoUUID}.${extension[1]}`);
				}

			}
		});
		return multer({ storage: storage });
	}

	initializeUpdateImage() {
		return this.router.use('/uploadImage',
			<any>this.checkIfFolderExists,
			this.multerMiddlewareUploadImage().single('file'),
			this.router.post('/', async (req: any, res: Response) => {
				try {

					if (!req.file) {
						return res.status(400).json({
							message: 'Incorrect image extension. Upload an image with following extension: png|jpeg|jpg|wav|tif|gif'
						})
					}

					const extractToken = req.headers.authorization.split(' ');
					if (extractToken.length < 1) {
						return res.status(400).json({
							message: 'Invalid authorization'
						})
					}

					const { tags, description, location, date } = req.body;
					const imageName = req.file.filename;
					const jwtToken = extractToken[1];
					const decode = <{ name: string, email: string }>this.jwtMiddleware.decodeToken(jwtToken);
					const result = await this.imageDataHandler.uploadImage(
						tags,
						description,
						location,
						date,
						decode.name,
						decode.email,
						imageName);

					return res.status(result.statusCode).json({
						message: result.message,
					});
				} catch (err) {
					return res.status(500).json({
						message: err.message,
					});
				}
			}));
	}

	initializeAddUpdateRating() {
		return this.router.post('/addRatting', async (req: any, res: Response) => {
			try {

				const extractToken = req.headers.authorization.split(' ');
				if (extractToken.length < 1) {
					return res.status(400).json({
						message: 'Invalid authorization'
					})
				}

				const { imageID, stars } = req.body;
				const jwtToken = extractToken[1];
				const decode = <{ name: string, email: string }>this.jwtMiddleware.decodeToken(jwtToken);

				const result = await this.imageDataHandler.addUpdateRating(
					imageID,
					stars,
					decode.name,
					decode.email);
				if (result.averageStar) {
					return res.status(result.statusCode).json({
						message: result.message,
						averageStar: result.averageStar
					});
				}

				return res.status(result.statusCode).json({
					message: result.message,
					token: jwtToken
				});
			} catch (err) {
				return res.status(500).json({
					message: err.message,
				});
			}
		})
	}

	initializeAddComment() {
		return this.router.post('/addComment', async (req: any, res: Response) => {
			try {

				const extractToken = req.headers.authorization.split(' ');
				if (extractToken.length < 1) {
					return res.status(400).json({
						message: 'Invalid authorization'
					})
				}

				const { imageID, comment } = req.body;
				const jwtToken = extractToken[1];
				const decode = <{ name: string, email: string }>this.jwtMiddleware.decodeToken(jwtToken);
				const result = await this.imageDataHandler.addComments(
					imageID,
					comment,
					decode.name,
					decode.email);

				return res.status(result.statusCode).json({
					message: result.message,
					token: jwtToken
				});
			} catch (err) {
				return res.status(500).json({
					message: err.message,
				});
			}
		})
	}

	initializeGetImageData() {
		return this.router.post('/getImages', async (req: any, res: Response) => {
			try {

				const {
					page,
					tags,
					location,
					fromData,
					toData,
					description,
					sortDate,
					sortLocation
				} = req.body;

				const result = await this.imageDataHandler.getImages(
					page,
					tags,
					location,
					fromData,
					toData,
					description,
					sortDate,
					sortLocation);

				if (result.imageData) {
					res.contentType('json');
					return res.send(result.imageData);
				}
				return res.status(result.statusCode).json({
					message: result.message,
				});
			} catch (err) {
				return res.status(500).json({
					message: err.message,
				});
			}
		})
	}

}