export interface IApiConfig {
  mongoUrl: string;
  jwtSecretKey: string;
  serverPort: number;
  photosPath: string,
}
