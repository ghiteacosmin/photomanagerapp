export interface IStarRating {
  stars: number,
  name: string,
  email: string,
}
