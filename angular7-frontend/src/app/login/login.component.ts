import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../authentication.service';
import { Router } from '@angular/router';

interface responseInterface {
  token: string;
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  error: string;
  constructor(private auth: AuthenticationService, private router: Router) { }

  ngOnInit() {
    if (this.auth.isLoggedIn()) {
      this.router.navigate(['dashboard']);
    }
  }

  onSubmit(email: string, password: string) {
    this.auth.login(email, password)
    .subscribe((result: responseInterface) => {
      const jwtToken = result.token;
      localStorage.setItem('userToken', jwtToken);
      return this.router.navigate(['dashboard']);
    }, (error) => {
      if (error.status >= 500) {
        return this.error = 'There is a server problem';
      }
      return this.error = error.error.message;
    })
  }
}
