import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule }   from '@angular/forms';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
import { LoginComponent } from './login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TokenInterceptorService } from './token-interceptor.service';
import { AuthenticationService } from './authentication.service';
import { UploadImageComponent } from './upload-image/upload-image.component';
import { ViewImageComponent } from './view-image/view-image.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { ImageService } from './image.service';
import { ImageRequestPipe } from './image-request.pipe';
import { StarRatingModule } from 'angular-star-rating';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    RegistrationComponent,
    LoginComponent,
    DashboardComponent,
    UploadImageComponent,
    ViewImageComponent,
    PageNotFoundComponent,
    ImageRequestPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    StarRatingModule.forRoot()
  ],
  providers: [AuthenticationService, ImageService, {
    provide: HTTP_INTERCEPTORS,
    useClass: TokenInterceptorService,
    multi: true
  }],
  bootstrap: [AppComponent]
})
export class AppModule { }
