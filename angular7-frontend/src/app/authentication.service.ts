import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  constructor(private http: HttpClient) { }

  login(email: string, password: string) {
    return this.http.post('/api/login', { email, password });
  }


  registration(email: string, password: string, name: string) {
    return this.http.post('/api/registration', {
      email, password, name
    });
  }
  
  isLoggedIn() {
    if (localStorage.getItem('userToken')) {
      return true;
    }
    return false;
  }

  logout() {
    return localStorage.removeItem('userToken');
  }
}
