import { Schema } from 'mongoose';
import { CommentsSchema } from './comments_schema';
import { StarRatingsSchema } from './star_rating_schema';

export const ImageSchema = new Schema({
  name: {
    type: String,
    minlength: 5,
    maxlength: 50
  },
  email: {
    type: String,
    minlength: 5,
    maxlength: 255,
  },
  img: String,
  tags: {
    type: [String],
  },
  description: {
    type: String,
    minlength: 1,
    maxlength: 255,
  },
  location: {
    type: String,
    minlength: 1,
    maxlength: 255,
  },
  date: {
    type: Date,
    default: Date.now()
  },
  comments: {
    type: [CommentsSchema]

  },
  starRating: {
    type: [StarRatingsSchema]
  },
  averageStar: {
    type: Number,
    default: 0
  }
});
