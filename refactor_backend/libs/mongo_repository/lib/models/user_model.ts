import { model, Model } from 'mongoose';
import { IUserModel } from '../../../interfaces';
import { UserSchema } from '../schemas/user_schema';

export const UserModel: Model<IUserModel> = model<IUserModel>('UserModel', UserSchema);
